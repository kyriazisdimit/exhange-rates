# exhange-rates

This project is created in order to provide some apis for the retrieve of 
the exchange rate between currencies and the conversion of them based on a given amount

The exposed apis are 3.

1. GET http://localhost:8080/exchange/v1 (must give as query parameter currency1 and currency 2 e.g.: http://localhost:8080/exchange/v1?currencyA=EUR&currencyB=USD)
2. GET http://localhost:8080/exchange/v1/base (must give as query parameter currency1  e.g.: http://localhost:8080/exchange/v1/base?currency=EUR) in this case it returns the exchange rate of the given currency with all the other available
3. GET http://localhost:8080/conversion/v1 (must give as query parameter currency1 and currency 2 and amount  e.g.: http://localhost:8080/conversion/v1?currencyA=EUR&currencyB=USD&amount=10) in this case it returns the conversion value of the given amount from the currency A to currency B, the currency B can be also a list of currencies form example you can ask the conversion of 10 euro to more than one currency in this case the url should be : http://localhost:8080/conversion/v1?currencyA=EUR&currencyB=USD,AFN,AED&amount=10

For more details about the apis please consult the swagger file in the folder exchange-rates/src/main/resources

# How to launch the application

1. Be sure that in the environment you are going to run the application you have installed both java 11 and mvn 3
2. Go to the project using a line command tool and execute the command mvn clean install
3. Then navigate into the target file that have been created
4. Execute the command java -jar exchange-rates.jar