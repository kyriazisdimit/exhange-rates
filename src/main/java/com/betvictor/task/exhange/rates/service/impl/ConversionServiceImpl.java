package com.betvictor.task.exhange.rates.service.impl;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import com.betvictor.task.exhange.rates.service.ConversionService;
import com.betvictor.task.exhange.rates.service.ExchangeRatioService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConversionServiceImpl implements ConversionService {

    private final ExchangeRatioService exchangeRatioService;
    private final ValidationService validationService;


    @Override
    public ResponseConversionDto convertAmountBetweenCurrencies(String currencyA, String currencyB, Double amount)
            throws InvalidValuesException {
        if (!StringUtils.isEmpty(currencyB) && !StringUtils.isBlank(currencyB)) {
            List<String> currencies = Arrays.asList(currencyB.split(","));
            ResponseConversionDto conversionDto = new ResponseConversionDto();
            conversionDto.setFrom(currencyA);
            conversionDto.setIncomingAmount(amount);
            if (currencies.size() == 1) {
                return convertAmountBetweenTwoCurrencies(currencyA, currencyB.toUpperCase(), amount, conversionDto);
            } else {
                return treatList(currencyA, currencies, amount, conversionDto);
            }
        } else {
            ErrorDetailDTO errorDetailDTO = new ErrorDetailDTO();
            errorDetailDTO.setErrorLevel(LogLevel.ERROR);
            errorDetailDTO.setTips("Please try with a different value for the currency B");
            errorDetailDTO.setErrorMessage("The giving value for the " + Constants.CURRENCY_B + " is invalid.");
            log.error("The giving value for the " + Constants.CURRENCY_B + " is invalid.");
            throw new InvalidValuesException(errorDetailDTO);
        }


    }

    private ResponseConversionDto convertAmountBetweenTwoCurrencies(String curA, String currencyB, Double amount,
                                                                    ResponseConversionDto conversionDto)
            throws InvalidValuesException {
        validationService.validateCurrency(currencyB.trim(), Constants.CURRENCY_B);
        ResponseExchangeRateDto rate = exchangeRatioService.getExchangeRatioForTwoCurrencies(curA.trim(),
                currencyB);
        BigDecimal value = BigDecimal.valueOf(amount).multiply(rate.getRate());
        conversionDto.setRate(rate.getRate());
        conversionDto.setTo(currencyB);
        conversionDto.setResult(value.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros());
        return conversionDto;
    }

    private ResponseConversionDto treatList(String curA, List<String> currencies, Double amount,
                                            ResponseConversionDto conversionDto) {
        List<ResponseConversionDto> conversions = new ArrayList<>();
        for (String currency : currencies) {
            ResponseConversionDto currentConversionDto = new ResponseConversionDto();
            try {
                validationService.validateCurrency(currency.trim(), Constants.CURRENCY_B);
                ResponseExchangeRateDto rate = exchangeRatioService.getExchangeRatioForTwoCurrencies(curA.trim(),
                        currency.toUpperCase());
                currentConversionDto.setRate(rate.getRate());
                currentConversionDto.setTo(currency.toUpperCase());
                BigDecimal value = BigDecimal.valueOf(amount).multiply(rate.getRate());
                currentConversionDto.setResult((value.setScale(2, RoundingMode.HALF_UP).stripTrailingZeros()));
                conversions.add(currentConversionDto);
            } catch (InvalidValuesException e) {
                currentConversionDto.setTo(currency);
                currentConversionDto.setFailMessage("The conversion for this currency wasn't possible");
                conversions.add(currentConversionDto);
            }
        }
        conversionDto.setConversions(conversions);
        return conversionDto;
    }

}
