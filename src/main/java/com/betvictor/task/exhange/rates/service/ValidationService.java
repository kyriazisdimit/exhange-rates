package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;

/**
 * This service is used to validate the input data
 */
public interface ValidationService {

    /**
     * Validate the input currency
     *
     * @param currency     the requested currency
     * @param currencyName the currency we are checking either currency A or currency B
     * @throws com.betvictor.task.exhange.rates.exceptions.InvalidValuesException
     */
    void validateCurrency(String currency, String currencyName) throws InvalidValuesException;

    /**
     * Validate the input amount who requested to be converted
     *
     * @param amount the requested amount
     * @throws com.betvictor.task.exhange.rates.exceptions.WrongAmountException if the requested amount is wrong that means less or equals to 0 or no accepted
     *                              value
     */
    void validateAmount(Double amount) throws WrongAmountException;
}
