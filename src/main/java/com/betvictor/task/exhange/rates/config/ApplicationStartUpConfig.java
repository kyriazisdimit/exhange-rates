package com.betvictor.task.exhange.rates.config;


import com.betvictor.task.exhange.rates.service.RetrieveExchangeRatioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * This event is executed as late as conceivably possible to indicate that
 * the application is ready to service requests.
 */
@Component
public class ApplicationStartUpConfig implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    private RetrieveExchangeRatioService exchangeRatioService;

    /**
     * This method is used in order to initiate the cache
     * after the start of the service
     */
    @Override
    public void onApplicationEvent(final ApplicationReadyEvent event) {
        exchangeRatioService.initCache();
    }


}
