package com.betvictor.task.exhange.rates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication(scanBasePackages = {"com.betvictor.task.exhange.rates"})
@EnableCaching
public class ExchangeRatesApplication {
    /**
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(ExchangeRatesApplication.class, args);
    }

}
