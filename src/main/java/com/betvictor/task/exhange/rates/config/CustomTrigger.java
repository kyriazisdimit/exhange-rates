package com.betvictor.task.exhange.rates.config;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.TriggerContext;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * This class is the trigger we will use in order
 * to plan the execution of our scheduler
 */
@RequiredArgsConstructor
public class CustomTrigger implements Trigger {

    private final Long frequency;
    private Date nextExecutionPlanned;

    @Override
    public Date nextExecutionTime(TriggerContext triggerContext) {
        LocalDateTime myTime = LocalDateTime.now().plusSeconds(frequency);
        nextExecutionPlanned = Date.from(myTime.atZone(ZoneId.systemDefault()).toInstant());
        return nextExecutionPlanned;
    }

    public String toString() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return this.nextExecutionPlanned != null ? dateFormat.format(this.nextExecutionPlanned) : "Not planned";
    }
}

