package com.betvictor.task.exhange.rates.exceptions;

import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import lombok.Getter;

@Getter
public class InvalidValuesException extends Exception {

    private final ErrorDetailDTO errorDetailDTO;

    public InvalidValuesException(ErrorDetailDTO error) {
        super(error.getErrorMessage());
        this.errorDetailDTO = error;
    }
}
