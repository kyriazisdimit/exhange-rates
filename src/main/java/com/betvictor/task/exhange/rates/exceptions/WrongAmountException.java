package com.betvictor.task.exhange.rates.exceptions;

import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import lombok.Getter;

@Getter
public class WrongAmountException extends Exception {

    private final ErrorDetailDTO errorDetailDTO;

    public WrongAmountException(ErrorDetailDTO error) {
        super(error.getErrorMessage());
        this.errorDetailDTO = error;

    }
}
