package com.betvictor.task.exhange.rates.constants;

import lombok.Getter;

@Getter
public final class Constants {

    public static final String GET_RATE_PATH = "/exchange/v1";
    public static final String GET_VALUE_PATH = "/conversion/v1";
    public static final String BASE_CURRENCY = "EUR";
    public static final String CURRENCY_A = "Currency A";
    public static final String CURRENCY_B = "Currency B";

    private Constants() throws IllegalAccessException {
        throw new IllegalAccessException("This class can't be initialized");
    }

}
