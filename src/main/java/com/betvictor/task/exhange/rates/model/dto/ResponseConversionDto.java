package com.betvictor.task.exhange.rates.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * The dto returned after the call of the api conversion
 * exposed from our service
 */
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseConversionDto implements Serializable {

    private String from;
    private Double incomingAmount;
    private String to;
    private BigDecimal rate;
    private BigDecimal result;
    private List<ResponseConversionDto> conversions;
    private String failMessage;

    public ResponseConversionDto(String from, String to, Double incomingAmount, BigDecimal result, BigDecimal rate) {
        this.from = from;
        this.to = to;
        this.result = result;
        this.incomingAmount = incomingAmount;
        this.rate = rate;
    }

    public ResponseConversionDto() {
    }


}
