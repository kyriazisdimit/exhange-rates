package com.betvictor.task.exhange.rates.model.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Dto which contains the currency and the exchange rate
 * info received from the call of the api
 */
@Setter
@Getter
public class RateDto implements Serializable {

    private String currency;
    private BigDecimal rate;

}
