package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;

/**
 * This service is responsible to do the calculations
 * and return to the controller the requested
 */
public interface ExchangeRatioService {

    /**
     * This method is called in order to give the exchange
     * rate between the requested two currencies
     *
     * @param currencyA the first currency
     * @param currencyB the second currency
     * @return @com.betvictor.task.exchange.rates.ResponseConversionDto
     */
    ResponseExchangeRateDto getExchangeRatioForTwoCurrencies(String currencyA, String currencyB);

    /**
     * This method is called in order to give the exchange
     * rate between the requested currency and all the others
     *
     * @param currencyA the requested currency
     * @return @com.betvictor.task.exchange.rates.ResponseConversionDto
     */
    ResponseExchangeRateDto getAllExchangeRatesForCurrency(String currencyA);

}
