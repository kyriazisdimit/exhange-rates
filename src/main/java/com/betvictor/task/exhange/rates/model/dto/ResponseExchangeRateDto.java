package com.betvictor.task.exhange.rates.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

/**
 * The dto returned after the call of the api exchange
 * exposed from our service
 */
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseExchangeRateDto implements Serializable {

    private String from;
    private String to;
    private BigDecimal rate;
    private Map<String, BigDecimal> rates;

    public ResponseExchangeRateDto(String from, String to, BigDecimal rate) {
        this.rate = rate;
        this.from = from;
        this.to = to;
    }

    public ResponseExchangeRateDto(String from, Map<String, BigDecimal> rates) {
        this.rates = rates;
        this.from = from;
    }
}
