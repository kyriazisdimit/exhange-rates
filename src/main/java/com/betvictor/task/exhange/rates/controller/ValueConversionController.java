package com.betvictor.task.exhange.rates.controller;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This controller expose the api for
 * the conversion of an amount between currencies
 */
public interface ValueConversionController {

    /**
     * Given currency A and currency B and amount return the conversion value into the
     * ResponseConversionDto
     *
     * @param currencyOne the starting currency of the amount
     * @param currencyTwo the second currency could be either a single value e.g.:USD
     *                    or a list of them concatenated with a ',' e.g.: USD,AFN,AED
     * @param amount      the requested amount to be transformed
     * @return ResponseConversionDto which contains all the necessary information
     * @throws com.betvictor.task.exhange.rates.exceptions.InvalidValuesException if the currency A or B is not correct
     * @throws com.betvictor.task.exhange.rates.exceptions.WrongAmountException   if the requested amount is wrong (less or equals to 0)
     */
    ResponseEntity<ResponseConversionDto> getValueConversionForCurrencies(
            @RequestParam(value = "currencyA", required = false) String currencyOne,
            @RequestParam(value = "currencyB", required = false) String currencyTwo,
            @RequestParam(value = "amount", required = false) Double amount)
            throws InvalidValuesException, WrongAmountException;

}
