package com.betvictor.task.exhange.rates.model.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.logging.LogLevel;

import java.io.Serializable;

/**
 * The dto we return in case of error
 */
@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class ErrorDetailDTO implements Serializable {

    private String errorMessage;

    private LogLevel errorLevel;

    private String tips;


}
