package com.betvictor.task.exhange.rates.service.impl;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import com.betvictor.task.exhange.rates.service.RetrieveExchangeRatioService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.logging.LogLevel;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class ValidationServiceImpl implements ValidationService {

    private final RetrieveExchangeRatioService ratioService;

    @Override
    public void validateCurrency(String currency, String currencyName) throws InvalidValuesException {
        if (StringUtils.isEmpty(currency) || ratioService.getCurrency(currency.trim().toUpperCase()).isEmpty()) {
            ErrorDetailDTO errorDetailDTO = new ErrorDetailDTO();
            errorDetailDTO.setErrorLevel(LogLevel.ERROR);
            errorDetailDTO.setTips("Please try with a different value for the " + currencyName);
            errorDetailDTO.setErrorMessage("The giving value : " + currency + " as " + currencyName + " is invalid.");
            log.error("The giving value : " + currency + " as " + currencyName + " is invalid.");
            throw new InvalidValuesException(errorDetailDTO);
        }
    }


    @Override
    public void validateAmount(Double amount) throws WrongAmountException {
        if (amount == null || amount <= 0) {
            ErrorDetailDTO errorDetailDTO = new ErrorDetailDTO();
            errorDetailDTO.setErrorLevel(LogLevel.ERROR);
            errorDetailDTO.setTips("Amount must be numeric and positive");
            errorDetailDTO.setErrorMessage("The giving value for the Amount is invalid.");
            log.error("The giving value for the Amount is invalid.");
            throw new WrongAmountException(errorDetailDTO);
        }
    }
}
