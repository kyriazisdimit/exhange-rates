package com.betvictor.task.exhange.rates.controller.interceptor;

import com.betvictor.task.exhange.rates.controller.ExchangeRateController;
import com.betvictor.task.exhange.rates.controller.ValueConversionController;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

/**
 * Exception Application Handler is used in order to
 * manage any wrong request or an internal exception
 * and to give the correct information to the user
 */
@RestControllerAdvice(assignableTypes = {ExchangeRateController.class, ValueConversionController.class})
@Slf4j
public class ApplicationExceptionHandler {


    @ExceptionHandler(value = {InvalidValuesException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDetailDTO> validationCurrencyException(InvalidValuesException ex, WebRequest req) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON).body(ex
                .getErrorDetailDTO());
    }

    @ExceptionHandler(value = {WrongAmountException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ErrorDetailDTO> validationAmountException(WrongAmountException ex, WebRequest req) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).contentType(MediaType.APPLICATION_JSON).body(ex
                .getErrorDetailDTO());
    }

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ResponseEntity<String> unhandledException(Exception ex, WebRequest req) {
        log.error("Unhandled exception has been thrown {}", ex.getMessage(), ex);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON).body(ex
                .getMessage());
    }

}
