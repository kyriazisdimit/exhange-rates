package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;

/**
 * This service is responsible to do the conversions between currencies
 */
public interface ConversionService {

    /**
     * This method is responsible to convert the requested amount
     * from the currency A to currency B
     *
     * @param currencyA the requested currency A
     * @param currencyB the requested currency B
     * @param amount    the requested amount
     * @return a @com.betvictor.task.exchange.rates.ResponseConversionDto with the associated information
     * @throws com.betvictor.task.exhange.rates.exceptions.InvalidValuesException if one of the currencies is wrong
     */
    ResponseConversionDto convertAmountBetweenCurrencies(String currencyA, String currencyB, Double amount)
            throws InvalidValuesException;


}
