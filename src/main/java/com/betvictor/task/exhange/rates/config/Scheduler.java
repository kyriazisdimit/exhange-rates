package com.betvictor.task.exhange.rates.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * Scheduler configuration to launch the scheduler task every minute
 */
@Slf4j
@EnableScheduling
@RequiredArgsConstructor
@Configuration
public class Scheduler implements SchedulingConfigurer {

    private final SchedulerTask schedulerTask;


    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.addTriggerTask(schedulerTask, new CustomTrigger(60L));
    }

}

