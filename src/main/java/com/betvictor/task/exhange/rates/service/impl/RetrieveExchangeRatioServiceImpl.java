package com.betvictor.task.exhange.rates.service.impl;

import com.betvictor.task.exhange.rates.model.dto.RateDto;
import com.betvictor.task.exhange.rates.service.RetrieveExchangeRatioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class RetrieveExchangeRatioServiceImpl implements RetrieveExchangeRatioService {

    private static final String EXCHANGE_RATES_URL = "https://api.exchangerate.host/latest";
    private static final String CACHE_RATES = "cacheRates";
    private static final String ALL_RATES = "allRates";

    private final CacheManager cacheManager;
    private final RestTemplate restTemplate;


    private List<RateDto> loadCache() {
        List<RateDto> ratesDto = new ArrayList<>();
        ResponseEntity<Object> result = restTemplate.getForEntity(EXCHANGE_RATES_URL, Object.class);
        if (result.getStatusCode().is2xxSuccessful()) {
            Map<String, Object> body = (Map<String, Object>) result.getBody();
            Map<String, Object> rates = (Map<String, Object>) Objects.requireNonNull(body).get("rates");
            for (Map.Entry<String, Object> a : rates.entrySet()) {
                RateDto o = new RateDto();
                o.setCurrency(a.getKey());
                o.setRate(new BigDecimal(a.getValue().toString()));
                ratesDto.add(o);
            }

        }
        return ratesDto;
    }

    @Override
    @Cacheable(value = CACHE_RATES, key = "'allRates'")
    public List<RateDto> initCache() {
        return loadCache();
    }

    @Override
    public void refreshCache() {
        List<RateDto> list = loadCache();
        if (!CollectionUtils.isEmpty(list)) {
            log.debug("The cache has been updated");
            Cache c = cacheManager.getCache(CACHE_RATES);
            Objects.requireNonNull(c).evict(ALL_RATES);
            c.put(ALL_RATES, list);
        }
    }

    @Override
    public Optional<RateDto> getCurrency(String curName) {
        Cache c = cacheManager.getCache(CACHE_RATES);
        List<RateDto> list = Objects.requireNonNull(c).get(ALL_RATES, List.class);
        return Objects.requireNonNull(list).
                stream().filter(a -> a.getCurrency().equals(curName)).findFirst();
    }

    public List<RateDto> getAll() {
        Cache c = cacheManager.getCache(CACHE_RATES);
        return Objects.requireNonNull(c).get(ALL_RATES, List.class);
    }

}
