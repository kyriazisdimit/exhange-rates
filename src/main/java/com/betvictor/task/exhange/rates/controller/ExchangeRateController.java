package com.betvictor.task.exhange.rates.controller;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This controller expose the api for
 * the retrieve of the exchange rates
 */
public interface ExchangeRateController {

    /**
     * This api give the exchange rate between two given currencies
     */
    @GetMapping
    ResponseEntity<ResponseExchangeRateDto> getExchangeRateForTwoCurrencies(
            @RequestParam(value = "currencyA", required = false) String currencyOne,
            @RequestParam(value = "currencyB", required = false) String currencyTwo) throws InvalidValuesException;

    /**
     * This api give the exchange rate between all the currencies and the given currency
     */
    @GetMapping
    ResponseEntity<ResponseExchangeRateDto> getExchangeRateForAllCurrencies(@RequestParam(value = "currency",
            required = false) String currency)
            throws InvalidValuesException;


}
