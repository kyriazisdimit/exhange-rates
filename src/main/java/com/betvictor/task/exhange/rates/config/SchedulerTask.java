package com.betvictor.task.exhange.rates.config;

import com.betvictor.task.exhange.rates.service.RetrieveExchangeRatioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * This class is the scheduler
 * which it will be executed every minute in order to refresh
 * the cache of the service
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class SchedulerTask implements Runnable {

    private final RetrieveExchangeRatioService exchangeRatioService;

    @Override
    public void run() {
        try {
            exchangeRatioService.refreshCache();
        } catch (Exception e) {
            log.error("Problem during the update of the cache nothing will be change for the moment", e);

        }
    }
}
