package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.model.dto.RateDto;

import java.util.List;
import java.util.Optional;

/**
 * This service is responsible to communicate
 * with the api in order to retrieve the exchange rates
 * and manage the cache memory
 */
public interface RetrieveExchangeRatioService {

    /**
     * This method is called during the initialization
     * of the service in order to load the cache
     *
     * @return the list that will retrieve during the call of the api
     * with the exchange rates with the base currency EUR
     */
    List<RateDto> initCache();

    /**
     * This method is called from the scheduler to
     * refresh the cache every single minute
     */
    void refreshCache();

    /**
     * This method is called in order to get the details of the exchange rate
     * for the requested currency having always as base currency the EUR
     * <p>
     * Example : if the requested currency is USD it will give the exchange rate of the USD with the EUR
     *
     * @param currency the currency for which we search the exchange rate
     * @return an optional object of type @com.betvictor.task.exchange.rates.RateDto with the data if exists
     * otherwise returns optional empty
     */
    Optional<RateDto> getCurrency(String currency);

    /**
     * This method is called in order to get the exchange rate for all the currencies
     * having always as base currency the EUR
     * <p>
     *
     * @return a list object of type @com.betvictor.task.exchange.rates.RateDto (in other words the cache)
     */
    List<RateDto> getAll();
}
