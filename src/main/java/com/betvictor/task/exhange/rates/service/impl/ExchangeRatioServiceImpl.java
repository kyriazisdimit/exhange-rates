package com.betvictor.task.exhange.rates.service.impl;

import com.betvictor.task.exhange.rates.model.dto.RateDto;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import com.betvictor.task.exhange.rates.service.RetrieveExchangeRatioService;
import com.betvictor.task.exhange.rates.service.ExchangeRatioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class ExchangeRatioServiceImpl implements ExchangeRatioService {

    private final RetrieveExchangeRatioService retrieveExchangeRatio;

    @Override
    public ResponseExchangeRateDto getExchangeRatioForTwoCurrencies(String currencyA, String currencyB) {
        Optional<RateDto> rateA = retrieveExchangeRatio.getCurrency(currencyA.trim());
        Optional<RateDto> rateB = retrieveExchangeRatio.getCurrency(currencyB.trim());
        if (rateA.isPresent() && rateB.isPresent()) {
            return new ResponseExchangeRateDto(currencyA, currencyB,
                    (rateB.get().getRate().divide(rateA.get().getRate(), 10, RoundingMode.HALF_UP)).stripTrailingZeros());
        }
        throw new RuntimeException("Problem during the retrieve of the exchange rate");
    }

    @Override
    public ResponseExchangeRateDto getAllExchangeRatesForCurrency(String currencyA) {
        Optional<RateDto> rateA = retrieveExchangeRatio.getCurrency(currencyA.trim());
        if (rateA.isPresent()) {
            Map<String, BigDecimal> answer = new HashMap<>();
            retrieveExchangeRatio.getAll().forEach(rate -> {
                if (!rate.getRate().equals(rateA.get().getRate())) {
                    answer.put(rate.getCurrency(),
                            (rate.getRate().divide(rateA.get().getRate(), 10, RoundingMode.HALF_UP)).stripTrailingZeros());
                }
            });
            return new ResponseExchangeRateDto(currencyA, answer);
        }
        throw new RuntimeException("Problem during the retrieve of the exchange rate");
    }

}
