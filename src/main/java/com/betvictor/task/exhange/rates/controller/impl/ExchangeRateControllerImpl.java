package com.betvictor.task.exhange.rates.controller.impl;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.controller.ExchangeRateController;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import com.betvictor.task.exhange.rates.service.ExchangeRatioService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = Constants.GET_RATE_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class ExchangeRateControllerImpl implements ExchangeRateController {

    @Autowired
    private ExchangeRatioService exchangeRatioService;

    @Autowired
    private ValidationService validationService;

    @Override
    @GetMapping
    public ResponseEntity<ResponseExchangeRateDto> getExchangeRateForTwoCurrencies(String currencyOne,
                                                                                   String currencyTwo)
            throws InvalidValuesException {
        validationService.validateCurrency(currencyOne, Constants.CURRENCY_A);
        validationService.validateCurrency(currencyTwo, Constants.CURRENCY_B);
        return ResponseEntity.status(HttpStatus.OK).
                body(exchangeRatioService.getExchangeRatioForTwoCurrencies(currencyOne.toUpperCase(),
                        currencyTwo.toUpperCase()));
    }

    @Override
    @GetMapping("/base")
    public ResponseEntity<ResponseExchangeRateDto> getExchangeRateForAllCurrencies(String currency)
            throws InvalidValuesException {
        validationService.validateCurrency(currency, Constants.CURRENCY_A);
        return ResponseEntity.status(HttpStatus.OK).
                body(exchangeRatioService.getAllExchangeRatesForCurrency(currency.toUpperCase()));
    }
}
