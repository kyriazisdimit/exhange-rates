package com.betvictor.task.exhange.rates.controller.impl;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.controller.ValueConversionController;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;
import com.betvictor.task.exhange.rates.service.ConversionService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = Constants.GET_VALUE_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class ValueConversionControllerImpl implements ValueConversionController {

    @Autowired
    private ConversionService conversionService;

    @Autowired
    private ValidationService validationService;

    @Override
    @GetMapping
    public ResponseEntity<ResponseConversionDto> getValueConversionForCurrencies(String currencyOne,
                                                                                 String currencyTwo,
                                                                                 Double amount) throws InvalidValuesException, WrongAmountException {
        validationService.validateCurrency(currencyOne, Constants.CURRENCY_A);
        validationService.validateAmount(amount);
        return ResponseEntity.status(HttpStatus.OK).
                body(conversionService.convertAmountBetweenCurrencies(currencyOne.toUpperCase(), currencyTwo, amount));
    }
}
