package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.RateDto;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import com.betvictor.task.exhange.rates.service.impl.ExchangeRatioServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ExchangeRatioServiceTest {

    @InjectMocks
    private ExchangeRatioServiceImpl conversionService;

    @Mock
    private RetrieveExchangeRatioService exchangeRatioService;

    /**
     * Input
     * The base currency EUR and the currency USD
     * EUR -> 1
     * USD -> 0.995389
     * <p>
     * Expected Results
     * <p>
     * from = EUR
     * to = USD
     * rate = 0.995389
     * the map rates must be null
     */
    @Test
    public void happyFlowWithBaseCurrency() {
        Mockito.when(exchangeRatioService.getCurrency("EUR")).thenReturn(createRateDto("EUR", new BigDecimal(1)));
        Mockito.when(exchangeRatioService.getCurrency("USD")).thenReturn(createRateDto("USD",
                new BigDecimal(0.995389)));
        ResponseExchangeRateDto dto = conversionService.getExchangeRatioForTwoCurrencies("EUR", "USD");

        Assertions.assertEquals("EUR", dto.getFrom());
        Assertions.assertEquals("USD", dto.getTo());
        Assertions.assertEquals("0.995389", dto.getRate().toString());
        Assertions.assertNull(dto.getRates());
    }

    /**
     * Input
     * The currency USD and the base currency EUR
     * USD -> 0.995389
     * EUR -> 1
     * Expected Results
     * <p>
     * from = USD
     * to = EUR
     * rate = 1.0046323598
     * the map rates must be null
     */
    @Test
    public void happyFlowWithBaseCurrencyAsSecond() {
        Mockito.when(exchangeRatioService.getCurrency("EUR")).thenReturn(createRateDto("EUR", new BigDecimal(1)));
        Mockito.when(exchangeRatioService.getCurrency("USD")).thenReturn(createRateDto("USD",
                new BigDecimal(0.995389)));
        ResponseExchangeRateDto dto = conversionService.getExchangeRatioForTwoCurrencies("USD", "EUR");

        Assertions.assertEquals("USD", dto.getFrom());
        Assertions.assertEquals("EUR", dto.getTo());
        Assertions.assertEquals("1.0046323598", dto.getRate().toString());
        Assertions.assertNull(dto.getRates());
    }

    /**
     * Input
     * The currency AFN and the currency USD
     * AFN -> 88.155879
     * USD -> 0.995389
     * Expected Results
     * <p>
     * from = AFN
     * to = USD
     * rate = 0.0112973974
     * the map rates must be null
     */
    @Test
    public void happyFlowWithNoBaseCurrency() {
        Mockito.when(exchangeRatioService.getCurrency("AFN")).thenReturn(createRateDto("AFN",
                new BigDecimal(88.155879)));
        Mockito.when(exchangeRatioService.getCurrency("USD")).thenReturn(createRateDto("USD",
                new BigDecimal(0.995932)));
        ResponseExchangeRateDto dto = conversionService.getExchangeRatioForTwoCurrencies("AFN", "USD");
        Assertions.assertEquals("AFN", dto.getFrom());
        Assertions.assertEquals("USD", dto.getTo());
        Assertions.assertEquals("0.0112973974", dto.getRate().toString());
        Assertions.assertNull(dto.getRates());
    }

    /**
     * Input
     * The base currency EUR
     * EUR -> 1
     * and asked the exchange rate for all the others in the cache
     * <p>
     * Expected Results
     * <p>
     * from = EUR
     * to is null
     * rate is null
     * the map rates has 2 lines
     * and contains the currencies USD:0.995932 and AFN:88.155879
     */
    @Test
    public void askedConversionForAllRatedWithBaseCurrency() throws InvalidValuesException {
        Mockito.when(exchangeRatioService.getAll()).thenReturn(createListRateDto());
        Mockito.when(exchangeRatioService.getCurrency("EUR")).thenReturn(createRateDto("EUR", new BigDecimal(1)));
        ResponseExchangeRateDto dto = conversionService.getAllExchangeRatesForCurrency("EUR");
        Assertions.assertNotNull(dto.getRates());
        Assertions.assertEquals(2, dto.getRates().size());
        Assertions.assertTrue(dto.getRates().containsKey("USD"));
        Assertions.assertTrue(dto.getRates().containsKey("AFN"));
        Assertions.assertEquals("0.995932", dto.getRates().get("USD").toString());
        Assertions.assertEquals("88.155879", dto.getRates().get("AFN").toString());
        Assertions.assertEquals("EUR", dto.getFrom());
        Assertions.assertNull(dto.getTo());
        Assertions.assertNull(dto.getRate());
    }

    private List<RateDto> createListRateDto() {
        List<RateDto> l = new ArrayList<>();
        RateDto dto2 = new RateDto();
        dto2.setCurrency("USD");
        dto2.setRate(new BigDecimal(0.995932));
        l.add(dto2);
        RateDto dto1 = new RateDto();
        dto1.setCurrency("AFN");
        dto1.setRate(new BigDecimal(88.155879));
        l.add(dto1);
        return l;
    }

    private Optional<RateDto> createRateDto(String rate, BigDecimal exchangeRate) {
        RateDto dto = new RateDto();
        dto.setCurrency(rate);
        dto.setRate(exchangeRate);
        return Optional.of(dto);
    }


}
