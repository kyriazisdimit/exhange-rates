package com.betvictor.task.exhange.rates.controller;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;
import com.betvictor.task.exhange.rates.controller.impl.ValueConversionControllerImpl;
import com.betvictor.task.exhange.rates.controller.interceptor.ApplicationExceptionHandler;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import com.betvictor.task.exhange.rates.service.ConversionService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class ConversionControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ValueConversionControllerImpl controller;

    @Mock
    private ConversionService conversionService;

    @Mock
    private ValidationService validationService;


    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ApplicationExceptionHandler(), controller).build();
    }

    /**
     * Input
     * The controller was called for the conversion between EUR and USD for the amount of 10
     *
     * <p>
     * Expected Results
     * <p>
     * http status 200
     * body ->
     * from = EUR
     * to = USD
     * rate = 0.995389
     * incomingAmount=10.0
     * result = 9.95
     */
    @Test
    public void happyFlow() throws Exception {
        Mockito.when(conversionService.convertAmountBetweenCurrencies(Mockito.eq("EUR"), Mockito.eq(
                "USD"), Mockito.eq(10.0)
        )).thenReturn(createConversionDto("EUR", "USD", 10.0, 9.95, BigDecimal.valueOf(0.995389)));

        ResponseEntity<ResponseConversionDto> answer =
                controller.getValueConversionForCurrencies("EUR", "USD", 10.0);

        Assertions.assertEquals(HttpStatus.OK, answer.getStatusCode());
        Assertions.assertNotNull(answer.getBody());
        Assertions.assertEquals("EUR", answer.getBody().getFrom());
        Assertions.assertEquals("USD", answer.getBody().getTo());
        Assertions.assertEquals("0.995389", answer.getBody().getRate().toString());
        Assertions.assertEquals("9.95", answer.getBody().getResult().toString());

    }

    /**
     * Input
     * The controller was called for the exchange rate for two currencies  but the asked amount is equals to 0 which
     * is invalid
     *
     * <p>
     * Expected Results
     * <p>
     * http status 400
     * body ->
     * {\"errorMessage\":\"The giving value for the Amount is invalid.\"," +
     * "\"errorLevel\":\"ERROR\",\"tips\":\"Amount must be numeric and positive\"}
     */
    @Test
    public void invalidAmountExceptionFlow() throws Exception {
        ErrorDetailDTO error = new ErrorDetailDTO();
        error.setTips("Amount must be numeric and positive");
        error.setErrorMessage("The giving value for the Amount is invalid.");
        error.setErrorLevel(LogLevel.ERROR);
        Mockito.doThrow(new WrongAmountException(error)).when(validationService).validateAmount(0.0);
        String url = "/conversion/v1";
        MvcResult result = mockMvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
                        .param("currencyA", "cur1")
                        .param("currencyB", "cur2")
                        .param("amount", "0")
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        String expectedAnswer = "{\"errorMessage\":\"The giving value for the Amount is invalid.\"," +
                "\"errorLevel\":\"ERROR\",\"tips\":\"Amount must be numeric and positive\"}";
        Assertions.assertEquals(expectedAnswer, content);

    }

    /**
     * Input
     * The controller was called for the conversion but the currency B is not documented
     *
     * <p>
     * Expected Results
     * <p>
     * http status 400
     * body ->
     * {\"errorMessage\":\"The giving value for the Currency B is invalid.\"," +
     * "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency B\"}
     */
    @Test
    public void invalidValuesExceptionFlow() throws Exception {
        ErrorDetailDTO error = new ErrorDetailDTO();
        error.setTips("Please try with a different value for the Currency A");
        error.setErrorMessage("The giving value for the Currency A is invalid.");
        error.setErrorLevel(LogLevel.ERROR);
        Mockito.doThrow(new InvalidValuesException(error)).
                when(validationService).validateCurrency(" ", Constants.CURRENCY_A);

        String url = "/conversion/v1";
        MvcResult result = mockMvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
                        .param("currencyA", " ")
                        .param("currencyB", "USD")
                        .param("amount", "10")
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        String expectedAnswer = "{\"errorMessage\":\"The giving value for the Currency A is invalid.\"," +
                "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency A\"}";
        Assertions.assertEquals(expectedAnswer, content);
    }

    private ResponseConversionDto createConversionDto(String from, String to,
                                                      Double incomingValue, Double value, BigDecimal rate) {
        return new ResponseConversionDto(from, to, incomingValue, new BigDecimal(value.toString()), rate);
    }

    /**
     * Input
     * The controller was called for the conversion between EUR and more currencies for the amount of 10
     *
     * <p>
     * Expected Results
     * <p>
     * http status 200
     * body ->
     * from = EUR
     * to = USD
     * rate = 0.995389
     * incomingAmount=10.0
     * result = 9.95
     */
    @Test
    public void happyFlowForMoreCurrencies() throws Exception {
        Mockito.when(conversionService.convertAmountBetweenCurrencies(Mockito.eq("EUR"), Mockito.eq(
                "USD,AFN,AED"), Mockito.eq(10.0)
        )).thenReturn(createConversionDto("EUR", "USD", 10.0, 9.95, BigDecimal.valueOf(0.995389)));

        ResponseEntity<ResponseConversionDto> answer =
                controller.getValueConversionForCurrencies("EUR", "USD,AFN,AED", 10.0);

        Assertions.assertEquals(HttpStatus.OK, answer.getStatusCode());
        Assertions.assertNotNull(answer.getBody());
        Assertions.assertEquals("EUR", answer.getBody().getFrom());
        Assertions.assertEquals("USD", answer.getBody().getTo());
        Assertions.assertEquals("0.995389", answer.getBody().getRate().toString());
        Assertions.assertEquals("9.95", answer.getBody().getResult().toString());

    }


}
