package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.service.impl.ValidationServiceImpl;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.logging.LogLevel;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ValidationServiceTest {

    @InjectMocks
    private ValidationServiceImpl underTest;

    @Mock
    private RetrieveExchangeRatioService ratioService;

    /**
     * Input
     * The asked amount is 0
     * <p>
     * Expected Results
     * <p>
     * An exception WrongAmountException must be thrown with :
     * <p>
     * tips = Amount must be numeric and positive
     * message = The giving value for the Amount is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionWithZeroMoney() {
        try {
            underTest.validateAmount(0.0);
        } catch (WrongAmountException e) {
            Assertions.assertEquals("Amount must be numeric and positive",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value for the Amount is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }
    }

    /**
     * Input
     * The asked amount is null (not provided)
     * <p>
     * Expected Results
     * <p>
     * An exception WrongAmountException must be thrown with :
     * <p>
     * tips = Amount must be numeric and positive
     * message = The giving value for the Amount is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionWithNullMoney() {
        try {
            underTest.validateAmount(null);
        } catch (WrongAmountException e) {
            Assertions.assertEquals("Amount must be numeric and positive",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value for the Amount is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }
    }

    /**
     * Input
     * The asked amount is -10
     * <p>
     * Expected Results
     * <p>
     * An exception WrongAmountException must be thrown with :
     * <p>
     * tips = Amount must be numeric and positive
     * message = The giving value for the Amount is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionWithNegativeValueForMoney() {
        try {
            underTest.validateAmount(0.0);
        } catch (WrongAmountException e) {
            Assertions.assertEquals("Amount must be numeric and positive",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value for the Amount is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }
    }

    /**
     * Input
     * The currency A is null and the currency USD
     * <p>
     * Expected Results
     * <p>
     * An exception InvalidValuesException must be thrown with :
     * <p>
     * tips = Please try with a different value for the Currency A
     * message = The giving value for the Currency A is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionWithNoCurrencyA() {
        try {
            underTest.validateCurrency(" ", Constants.CURRENCY_A);
        } catch (InvalidValuesException e) {
            Assertions.assertEquals("Please try with a different value for the Currency A",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value :   as Currency A is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }

    }

    /**
     * Input
     * The currency A it is documented, but it does not exist in our cache memory
     * <p>
     * Expected Results
     * <p>
     * An exception InvalidValuesException must be thrown with :
     * <p>
     * tips = Please try with a different value for the Currency A
     * message = The giving value for the Currency A is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionForAllRatedWithWrongCurrency() {
        Mockito.when(ratioService.getCurrency("UDS")).thenReturn(Optional.empty());
        try {
            underTest.validateCurrency("UDS", Constants.CURRENCY_A);
        } catch (InvalidValuesException e) {
            Assertions.assertEquals("Please try with a different value for the Currency A",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value : UDS as Currency A is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }
    }


}
