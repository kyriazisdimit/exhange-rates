package com.betvictor.task.exhange.rates.service;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.exceptions.WrongAmountException;
import com.betvictor.task.exhange.rates.model.dto.ResponseConversionDto;
import com.betvictor.task.exhange.rates.service.impl.ConversionServiceImpl;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.logging.LogLevel;

import java.math.BigDecimal;
import java.math.RoundingMode;

@ExtendWith(MockitoExtension.class)
public class ConversionServiceTest {

    @InjectMocks
    private ConversionServiceImpl conversionService;

    @Mock
    private ExchangeRatioService exchangeRatioService;

    @Mock
    private ValidationService validationService;

    /**
     * Input
     * The base currency EUR and the currency USD for the amount of 10
     * EUR -> 1
     * USD -> 0.995389
     * <p>
     * Expected Results
     * <p>
     * from = EUR
     * to = USD
     * rate = 0.995389
     * the result is 9.95
     * the incomingAmount is 10.0
     * conversion is null
     * fail message is null
     */
    @Test
    public void happyFlowConversionBetweenTwoCurrencies() throws InvalidValuesException, WrongAmountException {
        Mockito.when(exchangeRatioService.getExchangeRatioForTwoCurrencies("EUR", "USD"))
                .thenReturn(createExchangeRateDto("EUR", "USD", new BigDecimal(0.995389).setScale(10,
                        RoundingMode.HALF_UP).stripTrailingZeros()));
        ResponseConversionDto dto = conversionService.convertAmountBetweenCurrencies("EUR", "USD", 10.0);

        Assertions.assertEquals("EUR", dto.getFrom());
        Assertions.assertEquals("USD", dto.getTo());
        Assertions.assertEquals("0.995389", dto.getRate().toString());
        Assertions.assertEquals(10.0, dto.getIncomingAmount());
        Assertions.assertEquals(BigDecimal.valueOf(9.95), dto.getResult());
        Assertions.assertNull(dto.getConversions());
        Assertions.assertNull(dto.getFailMessage());
    }

    /**
     * Input
     * The base currency EUR and a list of currencies ("USD", "AFN","AED","") for the amount of 10
     * EUR -> 1
     * USD -> 0.995389
     * AFN -> 88.155879
     * AED -> 3.658121
     * "" -> ??
     * amount -> 10
     *
     * <p>
     * Expected Results
     * <p>
     * from = EUR
     * the incomingAmount is 10.0
     * to is null
     * rate is null
     * the result is null
     * fail message is null
     * conversions is not null
     * it contains 4 lines
     * 0 -> to:USD, 9.95:result, rate: 0.995389 all the other fields must be null
     * 1 -> to:AFN, 881.55:result, rate: 88.155879 all the other fields must be null
     * 2 -> to:AED, 36.58:result, rate: 3.658121 all the other fields must be null
     * 3-> all the fiels are null except to:"", failMessage: "The conversion for this currency wasn't possible"
     */
    @Test
    public void happyFlowConversionBetweenCurrencies() throws InvalidValuesException {
        Mockito.when(exchangeRatioService.getExchangeRatioForTwoCurrencies("EUR", "USD"))
                .thenReturn(createExchangeRateDto("EUR", "USD", new BigDecimal(0.995389).setScale(10,
                        RoundingMode.HALF_UP).stripTrailingZeros()));
        Mockito.when(exchangeRatioService.getExchangeRatioForTwoCurrencies("EUR", "AFN"))
                .thenReturn(createExchangeRateDto("EUR", "AFN", new BigDecimal(88.155879).setScale(10,
                        RoundingMode.HALF_UP).stripTrailingZeros()));
        Mockito.when(exchangeRatioService.getExchangeRatioForTwoCurrencies("EUR", "AED"))
                .thenReturn(createExchangeRateDto("EUR", "AED", new BigDecimal(3.658121).setScale(10,
                        RoundingMode.HALF_UP).stripTrailingZeros()));
        Mockito.doNothing().when(validationService).validateCurrency("USD", Constants.CURRENCY_B);
        Mockito.doNothing().when(validationService).validateCurrency("AFN", Constants.CURRENCY_B);
        Mockito.doNothing().when(validationService).validateCurrency("AED", Constants.CURRENCY_B);
        Mockito.doThrow(new InvalidValuesException(new ErrorDetailDTO())).when(validationService).validateCurrency(""
                , Constants.CURRENCY_B);


        ResponseConversionDto dto = conversionService.convertAmountBetweenCurrencies("EUR", "USD,AFN,AED, ", 10.0);

        Assertions.assertEquals("EUR", dto.getFrom());
        Assertions.assertEquals(10.0, dto.getIncomingAmount());

        Assertions.assertNull(dto.getTo());
        Assertions.assertNull(dto.getRate());
        Assertions.assertNull(dto.getResult());
        Assertions.assertNotNull(dto.getConversions());
        Assertions.assertEquals(4, dto.getConversions().size());
        Assertions.assertEquals("USD", dto.getConversions().get(0).getTo());
        Assertions.assertEquals("AFN", dto.getConversions().get(1).getTo());
        Assertions.assertEquals("AED", dto.getConversions().get(2).getTo());
        Assertions.assertEquals(" ", dto.getConversions().get(3).getTo());

        Assertions.assertEquals(BigDecimal.valueOf(9.95), dto.getConversions().get(0).getResult());
        Assertions.assertEquals(BigDecimal.valueOf(881.56), dto.getConversions().get(1).getResult());
        Assertions.assertEquals(BigDecimal.valueOf(36.58), dto.getConversions().get(2).getResult());
        Assertions.assertNull(dto.getConversions().get(3).getResult());

        Assertions.assertEquals("0.995389", dto.getConversions().get(0).getRate().toString());
        Assertions.assertEquals("88.155879", dto.getConversions().get(1).getRate().toString());
        Assertions.assertEquals("3.658121", dto.getConversions().get(2).getRate().toString());
        Assertions.assertNull(dto.getConversions().get(3).getRate());

        Assertions.assertNull(dto.getConversions().get(0).getFrom());
        Assertions.assertNull(dto.getConversions().get(1).getFrom());
        Assertions.assertNull(dto.getConversions().get(2).getFrom());
        Assertions.assertNull(dto.getConversions().get(3).getFrom());

        Assertions.assertNull(dto.getConversions().get(0).getIncomingAmount());
        Assertions.assertNull(dto.getConversions().get(1).getIncomingAmount());
        Assertions.assertNull(dto.getConversions().get(2).getIncomingAmount());
        Assertions.assertNull(dto.getConversions().get(3).getIncomingAmount());

        Assertions.assertNull(dto.getConversions().get(0).getFailMessage());
        Assertions.assertNull(dto.getConversions().get(1).getFailMessage());
        Assertions.assertNull(dto.getConversions().get(2).getFailMessage());
        Assertions.assertEquals("The conversion for this currency wasn't possible",
                dto.getConversions().get(3).getFailMessage());

    }


    /**
     * Input
     * The currency B is null and the currency USD
     * <p>
     * Expected Results
     * <p>
     * An exception InvalidValuesException must be thrown with :
     * <p>
     * tips = Please try with a different value for the Currency B
     * message = The giving value for the Currency B is invalid.
     * log level = ERROR
     */
    @Test
    public void askedConversionWithNoCurrencyB() throws WrongAmountException {
        ErrorDetailDTO error = new ErrorDetailDTO();
        error.setTips("Please try with a different value for the Currency B");
        error.setErrorMessage("The giving value for the Currency B is invalid.");
        error.setErrorLevel(LogLevel.ERROR);
        try {
            conversionService.convertAmountBetweenCurrencies("USD", " ", 10.0);
        } catch (InvalidValuesException e) {
            Assertions.assertEquals("Please try with a different value for the currency B",
                    e.getErrorDetailDTO().getTips());
            Assertions.assertEquals("The giving value for the Currency B is invalid.",
                    e.getErrorDetailDTO().getErrorMessage());
            Assertions.assertEquals(LogLevel.ERROR, e.getErrorDetailDTO().getErrorLevel());
        }
    }


    private ResponseExchangeRateDto createExchangeRateDto(String from, String to, BigDecimal value) {
        return new ResponseExchangeRateDto(from, to, value);
    }


}
