package com.betvictor.task.exhange.rates.controller;

import com.betvictor.task.exhange.rates.constants.Constants;
import com.betvictor.task.exhange.rates.controller.impl.ExchangeRateControllerImpl;
import com.betvictor.task.exhange.rates.controller.interceptor.ApplicationExceptionHandler;
import com.betvictor.task.exhange.rates.exceptions.InvalidValuesException;
import com.betvictor.task.exhange.rates.model.dto.ErrorDetailDTO;
import com.betvictor.task.exhange.rates.model.dto.ResponseExchangeRateDto;
import com.betvictor.task.exhange.rates.service.ExchangeRatioService;
import com.betvictor.task.exhange.rates.service.ValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.logging.LogLevel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class ExchangeRateControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private ExchangeRateControllerImpl controller;

    @Mock
    private ExchangeRatioService exchangeRatioService;

    @Mock
    private ValidationService validationService;


    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ApplicationExceptionHandler(), controller).build();
    }

    /**
     * Input
     * The controller was called for the exchange rate between two currencies EUR and USD
     *
     * <p>
     * Expected Results
     * <p>
     * http status 200
     * body ->
     * from = EUR
     * to = USD
     * rate = 0.995389
     */
    @Test
    public void happyFlow() throws Exception {
        Mockito.when(exchangeRatioService.getExchangeRatioForTwoCurrencies(Mockito.eq("EUR"), Mockito.eq(
                "USD")
        )).thenReturn(createExchangeResponseDto("EUR", "USD",
                new BigDecimal(0.995389).setScale(10, RoundingMode.HALF_UP).stripTrailingZeros()));

        ResponseEntity<ResponseExchangeRateDto> answer = controller.getExchangeRateForTwoCurrencies("EUR", "USD");

        Assertions.assertEquals(HttpStatus.OK, answer.getStatusCode());
        Assertions.assertNotNull(answer.getBody());
        Assertions.assertEquals("EUR", answer.getBody().getFrom());
        Assertions.assertEquals("USD", answer.getBody().getTo());
        Assertions.assertEquals("0.995389", answer.getBody().getRate().toString());

    }

    /**
     * Input
     * The controller was called for the exchange rate for two currencies  but the second one is empty
     *
     * <p>
     * Expected Results
     * <p>
     * http status 400
     * body ->
     * {\"errorMessage\":\"The giving value for the Currency B is invalid.\"," +
     * "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency B\"}
     */
    @Test
    public void invalidValuesExceptionFlow() throws Exception {
        ErrorDetailDTO error = new ErrorDetailDTO();
        error.setTips("Please try with a different value for the Currency B");
        error.setErrorMessage("The giving value for the Currency B is invalid.");
        error.setErrorLevel(LogLevel.ERROR);
        Mockito.doNothing().
                when(validationService).validateCurrency("cur1", Constants.CURRENCY_A);
        Mockito.doThrow(new InvalidValuesException(error)).
                when(validationService).validateCurrency(" ", Constants.CURRENCY_B);

        String url = "/exchange/v1";
        MvcResult result = mockMvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
                        .param("currencyA", "cur1")
                        .param("currencyB", " ")
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();

        String content = result.getResponse().getContentAsString();
        String expectedAnswer = "{\"errorMessage\":\"The giving value for the Currency B is invalid.\"," +
                "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency B\"}";
        Assertions.assertEquals(expectedAnswer, content);

    }

    /**
     * Input
     * The controller was called for the exchange rate for all the currencies for the currency EUR
     *
     * <p>
     * Expected Results
     * <p>
     * http status 200
     * body ->
     * from = EUR
     * to = USD
     * rate = 0.995389
     */
    @Test
    public void happyFlowForMoreCurrencies() throws Exception {
        Mockito.when(exchangeRatioService.getAllExchangeRatesForCurrency(Mockito.eq("EUR"))
        ).thenReturn(createExchangeResponseDto("EUR", "USD",
                new BigDecimal(0.995389).setScale(10, RoundingMode.HALF_UP).stripTrailingZeros()));

        ResponseEntity<ResponseExchangeRateDto> answer = controller.getExchangeRateForAllCurrencies("EUR");

        Assertions.assertEquals(HttpStatus.OK, answer.getStatusCode());
        Assertions.assertNotNull(answer.getBody());
        Assertions.assertEquals("EUR", answer.getBody().getFrom());
        Assertions.assertEquals("USD", answer.getBody().getTo());
        Assertions.assertEquals("0.995389", answer.getBody().getRate().toString());

    }

    private ResponseExchangeRateDto createExchangeResponseDto(String from, String to,
                                                              BigDecimal value) {
        return new ResponseExchangeRateDto(from, to, value);
    }

    /**
     * Input
     * The controller was called for the exchange rate for all currencies but the asked currency is an empty value
     *
     * <p>
     * Expected Results
     * <p>
     * http status 400
     * body ->
     * {\"errorMessage\":\"The giving value for the Currency A is invalid.\"," +
     * "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency A\"}
     */
    @Test
    public void invalidValuesExceptionFlowForMoreCurrencies() throws Exception {
        ErrorDetailDTO error = new ErrorDetailDTO();
        error.setTips("Please try with a different value for the Currency A");
        error.setErrorMessage("The giving value for the Currency A is invalid.");
        error.setErrorLevel(LogLevel.ERROR);
        Mockito.doThrow(new InvalidValuesException(error)).
                when(validationService).validateCurrency("EUR", Constants.CURRENCY_A);

        String url = "/exchange/v1/base";
        MvcResult result = mockMvc.perform(get(url)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{}")
                        .param("currency", "EUR")
                )
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andReturn();
        String content = result.getResponse().getContentAsString();
        String expectedAnswer = "{\"errorMessage\":\"The giving value for the Currency A is invalid.\"," +
                "\"errorLevel\":\"ERROR\",\"tips\":\"Please try with a different value for the Currency A\"}";
        Assertions.assertEquals(expectedAnswer, content);
    }

}
